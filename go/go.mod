module benchmarks

go 1.22.3

require (
	github.com/grafana/regexp v0.0.0-20240518133315-a468a5bfb3bc
	github.com/wasilibs/go-re2 v1.5.3
)

require github.com/tetratelabs/wazero v1.7.1 // indirect
