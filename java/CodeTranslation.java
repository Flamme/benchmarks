import java.io.IOException;import java.nio.file.Files;
import java.nio.file.Paths;import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeTranslation {
  public static void main(String[] args) {
    String content;

    try {
      content = new String(Files.readAllBytes(Paths.get("../common/big.txt")), "UTF-8");
    } catch (IOException e) {
      System.out.println("Error reading file: " + e.getMessage());
      return;
    }

    Pattern regexp = Pattern.compile("(?:header|world)");
    long startTime = System.nanoTime();
    Matcher matcher = regexp.matcher(content);
    int count = 0;

    while (matcher.find()) {
        count++;
    }

    long endTime = System.nanoTime();
    System.out.println("Elapsed time: " + (endTime - startTime) / 1000000.0 + " ms");

    System.out.println(count);
  }
}
