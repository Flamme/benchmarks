# Benchmarks

## node 20.11.1

```
Time (mean ± σ):      52.4 ms ±   7.1 ms    [User: 38.7 ms, System: 13.5 ms]
Range (min … max):    46.2 ms … 100.2 ms    100 runs
```

## go 1.22.3

### standard library

```
Time (mean ± σ):     234.0 ms ±  19.2 ms    [User: 228.8 ms, System: 5.0 ms]
Range (min … max):   222.4 ms … 331.6 ms    100 runs
```

### github.com/grafana/regexp

```
Time (mean ± σ):     231.4 ms ±  17.8 ms    [User: 227.5 ms, System: 4.1 ms]
Range (min … max):   221.0 ms … 327.8 ms    100 runs
```

### github.com/wasilibs/go-re2

```
Time (mean ± σ):     487.6 ms ±  32.6 ms    [User: 479.2 ms, System: 20.9 ms]
Range (min … max):   445.0 ms … 582.3 ms    100 runs
```

## dart 3.4.0

```
Time (mean ± σ):      35.6 ms ±   4.6 ms    [User: 29.8 ms, System: 6.2 ms]
Range (min … max):    32.0 ms …  67.0 ms    100 runs
```

## java 1.8.0_402

```
Time (mean ± σ):     189.7 ms ±  18.2 ms    [User: 210.2 ms, System: 22.6 ms]
Range (min … max):   170.3 ms … 268.0 ms    100 runs
```
