import fs from "node:fs";

const regexp = /(?:header|world)/g;
const content = fs.readFileSync("../common/big.txt", "utf-8");

export default function regexpBenchmark() {
  const result = content.match(regexp);
  console.log(result.length);
}
