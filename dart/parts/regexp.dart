import 'dart:io';

RegExp _regexp = new RegExp(r'(?:header|world)');
File _file = new File('../common/big.txt');
String _fileContent = _file.readAsStringSync();

void benchTest() {
  var matches = _regexp.allMatches(_fileContent).map((m) => m.group(0)).toList();
  print(matches.length);
}
