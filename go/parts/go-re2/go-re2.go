package gore2

import (
	"fmt"
	regexp "github.com/wasilibs/go-re2"
	"os"
)

var r = regexp.MustCompile(`(?:header|world)`)
var b, err = os.ReadFile("../common/big.txt")
var s = string(b)

func BenchTest() {
	words := r.FindAllString(s, -1)
	fmt.Println(len(words))
}
